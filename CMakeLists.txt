cmake_minimum_required(VERSION 3.11)
project(void
  LANGUAGES C
  VERSION 0.1.0)

list(APPEND CMAKE_MODULE_PATH ${PROJECT_SOURCE_DIR}/cmake)
include(Dependency)

set(CMAKE_EXPORT_COMPILE_COMMANDS OFF)

set(DEPS_DIR ${PROJECT_SOURCE_DIR}/deps)

find_package(OpenGL REQUIRED)
find_package(Threads REQUIRED)

git(
  OpenAL
  https://github.com/kcat/openal-soft
  ce6076091bac3c00cd10803916e8911495580bd0)

git(
  GLFW
  https://github.com/glfw/glfw
  0be4f3f75aebd9d24583ee86590a38e741db0904)

# Override build settings for GLFW and OpenAL
cache(GLFW_BUILD_EXAMPLES OFF BOOL)
cache(GLFW_BUILD_TESTS OFF BOOL)
cache(GLFW_BUILD_DOCS OFF BOOL)

cache(ALSOFT_EXAMPLES OFF BOOL)
cache(ALSOFT_UTILS OFF BOOL)
cache(ALSOFT_TEST OFF BOOL)

add_library(glad STATIC)
target_sources(glad PRIVATE ${DEPS_DIR}/glad/glad.c)
target_include_directories(glad PUBLIC ${DEPS_DIR}/glad/include)

add_library(TOML)
target_sources(TOML PRIVATE ${DEPS_DIR}/tomlc99/toml.c)
target_include_directories(TOML PUBLIC ${DEPS_DIR}/tomlc99)

add_executable(void)
target_include_directories(void PRIVATE ${DEPS_DIR}/stb STATIC)
target_compile_features(void PRIVATE c_std_11)
target_compile_options(void PRIVATE
  $<$<PLATFORM_ID:MinGW>:-static-libgcc>)
target_sources(void
  PRIVATE
    src/main.c
    src/render.c
    src/audio.c
    src/tmath.c
    src/input.c
    src/timespec.c
    src/user.c
    src/util.c)

target_link_libraries(void
  PUBLIC
    OpenAL
    OpenGL::GL
    OpenGL::GLU
    GLFW::GLFW
    Threads::Threads
    TOML
    glad)

file(COPY ${PROJECT_SOURCE_DIR}/res
  DESTINATION ${PROJECT_BINARY_DIR})
