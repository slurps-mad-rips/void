#ifndef ALC_ENUMERATE_ALL_EXT
#define ALC_DEFAULT_ALL_DEVICES_SPECIFIER        0x1012
#define ALC_ALL_DEVICES_SPECIFIER                0x1013
#endif

#ifndef ALC_EXT_EFX
#define ALC_EFX_MAJOR_VERSION                    0x20001
#define ALC_EFX_MINOR_VERSION                    0x20002
#define ALC_MAX_AUXILIARY_SENDS                  0x20003
#endif

#include "audio.h"

#ifdef CACHE_AUDIO_FILES
a_file* a_gen_file(const char* fp, a_buffer* buffer){
    audioFiles[audioFileCount] = (a_file){fp, buffer};
    audioFileCount ++;
    return &audioFiles[audioFileCount];
}

int a_get_fileIndex(const char* fp){
    for(int i=0;i<audioFileCount;i++){
        if(strcmp(audioFiles[i].path, fp) == 0){
            return i;
        }
    }
    return -1;
}

a_file* a_get_file(const char* fp){
    int index = a_get_fileIndex(fp);
    if(index != -1){
        return &audioFiles[index];
    }
    return NULL;
}

void a_destroy_file(const char* fp){
    int index = a_get_fileIndex(fp);

    if(index != -1){
        free(audioFiles[index].buffer);
        for(int i=index;i<audioFileCount-1;i++){
            audioFiles[i] = audioFiles[i+1];
        }
        audioFileCount --;
    }
}

a_buffer* a_get_buffer(const char* fp){
    a_file* file = a_get_file(fp);
    if(file == NULL){
        a_buffer buffer = a_create_buffer(fp);
        file = a_gen_file(fp, &buffer);
    }
    return file->buffer;
}

a_file** a_load_sfx(const char** fp, int count){
    a_file** files = (a_file**)malloc(sizeof(a_file) * count);

    for(int i=0;i<count;++i){
        if(fp[i] != NULL){
            a_buffer buffer = a_create_buffer(fp[i]);
            if(buffer.id != 0 && audioFileCount < MAX_AUDIO_FILES){
                audioFiles[audioFileCount].buffer = &buffer;
                audioFiles[audioFileCount].path = fp[i];
                files[i] = &audioFiles[audioFileCount];
                audioFileCount ++;
            }
        }
    }

    return files;
}

void a_destroy_file_cache(){
    for(int i=0;i<audioFileCount;++i){
        a_destroy_buffer(*audioFiles[i].buffer);
    }
}
#endif

bool a_load_devices(){
    char* deviceList;

    if(alcIsExtensionPresent(NULL, "ALC_ENUMERATE_ALL_EXT") != AL_FALSE){
        deviceList = (char*)alcGetString(NULL, ALC_ALL_DEVICES_SPECIFIER);
    }else{
        deviceList = (char*)alcGetString(NULL, ALC_DEVICE_SPECIFIER);
    }

    if(!deviceList){
        printf("No device list\n");
    }

    char* start = &deviceList[0];

    char** list;
    int count = 0;
    int totalSize = 0;
    while(*deviceList != '\0'){
        ++count;
        size_t len = strlen(deviceList) + 1;
        totalSize += sizeof(char) * len;
        deviceList += len;
    }

    deviceList = start;
    list = malloc(sizeof(char) * totalSize);

    int index = 0;
    while(*deviceList != '\0'){
        size_t len = strlen(deviceList) + 1;
        list[index] = deviceList;
        deviceList += len;
        ++index;
    }

    a_device_list = list;
    a_device_list_count = count;

    return true;
}

bool a_create_context(const char* deviceName){
    ALCdevice* device = NULL;
    const char* tmpName = NULL;

    if(deviceName != NULL){
        device = alcOpenDevice(deviceName);
    }

    if(device == NULL){
        printf("Unable to open device: %s. Opening default device.\n", deviceName);

        device = alcOpenDevice(NULL);

        tmpName = alcGetString(device, ALC_DEVICE_SPECIFIER);
    }

    ALCcontext* context = alcCreateContext(device, NULL);

    if(!alcMakeContextCurrent(context)){
        printf("Error creating OpenAL Context\n");
        return false;
    }
    a_Context = (a_context){context, device};

    for(int i=0;i<MAX_QUICK_SFX;++i){
        a_qsfx[i].id = i;
        a_qsfx[i].source = a_create_source((vec3){0.f}, 10.f, 0);
    }

    return true;
}

void a_destroy_context(){
    if(a_Context.context == NULL){
        return;
    }

    for(int i=0;i<MAX_QUICK_SFX;++i){
        alDeleteSources(1, &a_qsfx[i].source.id);
    }

    alcDestroyContext(a_Context.context);
    alcCloseDevice(a_Context.device);
}

const char** a_get_devices(int* count){
    const char** deviceNames = malloc(sizeof(const char*) * a_device_list_count);
    for(int i=0;i<a_device_list_count; ++i){
        deviceNames[i] = a_device_list[i];
    }
    *count = a_device_list_count;
    return deviceNames;
}

void a_swap_device(const char* deviceName){
    a_allow = false;

    alcDestroyContext(a_Context.context);
    alcCloseDevice(a_Context.device);

    ALCdevice* device = NULL;
    if(deviceName != NULL){
        for(int i=0;i<a_device_list_count;++i){
            if(strcmp(a_device_list[i], deviceName) == 0){
                deviceName = a_device_list[i];
                break;
            }
        }
    }
    alcOpenDevice(deviceName);

    ALCcontext* context = NULL;
    context = alcCreateContext(device, NULL);
    alcMakeContextCurrent(context);

    a_Context.context = context;
    a_Context.device = device;

    a_allow = true;
}

void a_clean_layers(a_layer** layers, int count){
    for(int i=0;i<count;++i){
        a_clean_sources(layers[i]->sources, layers[i]->sourceCount);
        free(layers[i]->sources);
    }
}

void a_clean_sources(a_source* sources, int count){
    for(int i=0;i<count;++i){
        alDeleteSources(1, &sources[i].id);
    }
}

void a_clean_buffers(a_buffer* buffers, int count){
    for(int i=0;i<count;++i){
        alDeleteBuffers(1, &buffers[i].id);
    }
}

a_layer* a_create_layer(const char* name, a_source* sources, int length){
    if(name == NULL){
        return NULL;
    }

    a_layers[a_layer_count]        = malloc(sizeof(a_layer));
    a_layers[a_layer_count]->name  = name;
    a_layers[a_layer_count]->id    = a_layer_count;
    a_layers[a_layer_count]->gain  = 1.0f;
    a_layers[a_layer_count]->state = LAYER_STOPPED;

    if(sources != NULL && length > 0 && length < MAX_AUDIO_SOURCES_PER_LAYER){
        for(int i=0;i<length;++i){
            a_layers[a_layer_count]->sources[i] = sources[i];
        }
        a_layers[a_layer_count]->sourceCount = length;
    }

    a_layer_count ++;

    return a_layers[a_layer_count-1];
}

int a_get_layer_index(const char* name){
    for(int i=0;i<a_layer_count;++i){
        if(strcmp(a_layers[i]->name, name) == 0){
            return i;
        }
    }
    return -1;
}

void a_destroy_layer(int index){
    a_layer* l = a_layers[index];

    for(int i=index;i<a_layer_count-1;++i){
        a_layers[i] = a_layers[i+1];
    }

    a_layer_count --;
    free(l->sources);
    free(l);
}

void a_set_layer_gain(int index, float gain){
    if(a_layer_count < index){
        return;
    }

    for(int i=0;i<a_layers[index]->sourceCount;++i){
        alSourcef(a_layers[index]->sources[i].id, AL_GAIN, gain);
        a_layers[index]->sources[i].gain = gain;
    }

    a_layers[index]->gain = gain;
}

float a_get_layer_gain(int index){
    return a_layers[index]->gain;
}

void a_play_layer(int index){
    a_layers[index]->state = LAYER_PLAYING;
    for(int i=0;i<a_layers[index]->sourceCount;++i){
        a_play_source(&a_layers[index]->sources[i]);
    }
}

void a_attach_buffer(a_source* source, a_buffer* buffer){
    alSourcei(source->id, AL_BUFFER, buffer->id);
    source->hasSound = true;
}

void a_detach_buffer(a_source* source){
    alSourcei(source->id, AL_BUFFER, 0);
    source->hasSound = false;
}

a_buffer a_load_effect(a_effect* effect){
    return a_create_buffer(effect->path.path);
}

a_buffer a_create_buffer(const char* path){
    if(path == NULL){
        printf("NULL Audio Buffer.\n");
        return (a_buffer){0, false, 0, 0, 0};
    }
    int id;

    alGenBuffers(1, &id);

    //TODO variant sample size
    int sampleSize = 16;

    int channels, len, rate;
    short* data;

    stb_vorbis* vorbis;
    vorbis = stb_vorbis_open_filename(path, NULL, NULL);

    if(!vorbis){
        printf("Unable to open audio file with vorbis: %s\n", path);
        return (a_buffer){0};
    }

    stb_vorbis_info info = stb_vorbis_get_info(vorbis);

    int format;
    if(info.channels == 1){
        switch(sampleSize){
            case 8:
                format = AL_FORMAT_MONO8;
                break;
            case 16:
                format = AL_FORMAT_MONO16;
                break;
            default:
                printf("Unsupported format: channels: %i sampleSize: %i\n", info.channels, sampleSize);
                break;
        }
    }else if(info.channels == 2){
        switch(sampleSize){
            case 8:
                format = AL_FORMAT_STEREO8;
                break;
            case 16:
                format = AL_FORMAT_STEREO16;
                break;
            default:
                printf("Unsupported format: channels: %i sampleSize: %i\n", info.channels, sampleSize);
                break;
        }
    }

    unsigned int totalSamples;
    bool loop = false;

    totalSamples = (unsigned int)stb_vorbis_stream_length_in_samples(vorbis);
    int sampleCount = (MAX_MUSIC_RUNTIME > totalSamples) ? totalSamples : MAX_MUSIC_RUNTIME;

    void* pcm = calloc(totalSamples * sampleSize/8*info.channels, 1);

    int numSamples = stb_vorbis_get_samples_short_interleaved(vorbis, info.channels, (short*)pcm, sampleCount*info.channels);

    alBufferData(id, format, pcm, sampleCount*(sampleSize/8*info.channels), info.sample_rate);

    free(pcm);

    stb_vorbis_close(vorbis);

    return (a_buffer){id, true, channels, len, rate};
}

a_buffer* a_create_buffers(const char** paths, int pCount, int* bCount){
    if(pCount == 0){
        return NULL;
    }

    int bufferedCount;
    a_buffer* buffered = malloc(sizeof(a_buffer) * pCount);
    for(int i=0;i<pCount;i++){
        if(buffered->buffered == true){
            bufferedCount ++;
        }
    }

    *bCount = bufferedCount;

    return buffered;
}

void a_destroy_buffer(a_buffer buffer){
    alDeleteBuffers(1, &buffer.id);
}

a_stream a_create_stream(int sampleRate, int sampleSize, int channels){
    a_stream stream = {0};
    stream.sampleRate = sampleRate;
    stream.sampleSize = sampleSize;
    if((channels > 0) && (channels < 3)){
        stream.channels = channels;
    }else{
        printf("Invalid channel count in audio stream: %i\n", channels);
    }

    if(channels == 1){
        switch(sampleSize){
            case 8: stream.format = AL_FORMAT_MONO8; break;
            case 16: stream.format = AL_FORMAT_MONO16; break;
            default: printf("Invalid sample size in stream %i, all sizese must be multiple of 8.\n", sampleSize);
        }
    }else if(channels == 2){
        switch(sampleSize){
            case 8: stream.format = AL_FORMAT_STEREO8; break;
            case 16: stream.format = AL_FORMAT_STEREO16; break;
            default: printf("Invalid sample size in stream %i, all sizese must be multiple of 8.\n", sampleSize);
        }
    }

    alGenSources(1, &stream.source);
    alSourcef(stream.source, AL_PITCH, 1.0f);
    alSourcef(stream.source, AL_GAIN,  1.0f);
    alSource3f(stream.source, AL_POSITION, 0.0f, 0.0f, 0.0f);
    alSource3f(stream.source, AL_VELOCITY, 0.0f, 0.0f, 0.0f);

    alGenBuffers(2, stream.buffers);

    void* pcm = calloc(MAX_MUSIC_RUNTIME * stream.sampleSize/8*stream.channels, 1);
    for(int i=0;i<2;++i){
        alBufferData(stream.buffers[i], stream.format, pcm, stream.sampleSize/8*stream.channels, stream.sampleRate);
    }

    free(pcm);
    alSourceQueueBuffers(stream.source, 2, stream.buffers);
    return stream;
}

void a_destroy_stream(a_stream stream){
    alSourceStop(stream.source);

    int queued = 0;
    alGetSourcei(stream.source, AL_BUFFERS_QUEUED, &queued);

    ALuint buffer = 0;
    while(queued > 0){
        alSourceUnqueueBuffers(stream.source, 1, &buffer);
        queued --;
    }

    alDeleteSources(1, &stream.source);
    alDeleteBuffers(2, stream.buffers);
}

void a_update_stream(a_stream stream, const void* data, int sampleCount){
    ALuint buffer = 0;
    alSourceUnqueueBuffers(stream.source, 1, &buffer);
    if(alGetError() != AL_INVALID_VALUE){
        alBufferData(buffer, stream.format, data, sampleCount*(stream.sampleSize/8*stream.channels), stream.sampleRate);
        alSourceQueueBuffers(stream.source, 1, &buffer);
    }else{
        printf("Audio buffer not available for unqueueing: %i\n", buffer);
    }
}

a_music* a_create_music(const char* path){
    a_music* music = (a_music*)malloc(sizeof(a_music));
    music->vorbis = stb_vorbis_open_filename(path, NULL, NULL);
    if(music->vorbis == NULL){
        printf("Audio File could not be opened: %s\n", path);
    }else{
        stb_vorbis_info info = stb_vorbis_get_info(music->vorbis);

        music->stream = a_create_stream(info.sample_rate, 16, info.channels);

        if(music->stream.source == 0){
            printf("Unable to create music stream for: %s\n", path);
        }

        music->totalSamples = (unsigned int)stb_vorbis_stream_length_in_samples(music->vorbis);
        music->samplesLeft = music->totalSamples;
        music->loop = false;
    }

    return music;
}

bool a_update_music(a_music* music){
    ALenum state;
    ALint processed = 0;
    alGetSourcei(music->stream.source, AL_SOURCE_STATE, &state);
    alGetSourcei(music->stream.source, AL_BUFFERS_PROCESSED, &processed);

    if(processed > 0){
        bool ending = false;
        void* pcm = calloc(MAX_MUSIC_RUNTIME * music->stream.sampleSize/8*music->stream.channels, 1);
        int numToProcess = processed;
        int sampleCount = 0;

        for(int i=0;i<numToProcess;++i){
            if(music->samplesLeft >= MAX_MUSIC_RUNTIME){
                sampleCount = MAX_MUSIC_RUNTIME;
            }else{
                sampleCount = music->samplesLeft;
            }

            int numSamples = stb_vorbis_get_samples_short_interleaved(music->vorbis, music->stream.channels, (short*)pcm, sampleCount*music->stream.channels);
            a_update_stream(music->stream, pcm, sampleCount);
            music->samplesLeft -= sampleCount;

            if(music->samplesLeft <= 0){
                ending = true;
                break;
            }
        }

        free(pcm);

        if(ending){
            a_stop_music(music);
            if(music->loop){
                a_play_music(music);
            }
        }
    }
}

void a_destroy_music(a_music* music){
    a_destroy_stream(music->stream);
    stb_vorbis_close(music->vorbis);
    free(music);
}

float a_get_music_time(a_music* music){
    unsigned int samples = music->totalSamples - music->samplesLeft;
    return (float)samples / music->stream.sampleRate;
}

float a_get_music_len_time(a_music* music){
    return (float)(music->totalSamples/music->stream.sampleRate);
}

void a_play_music(a_music* music){
    alSourcePlay(music->stream.source);
}

void a_stop_music(a_music* music){
    alSourceStop(music->stream.source);
    stb_vorbis_seek_start(music->vorbis);
    music->samplesLeft = music->totalSamples;
}

void a_resume_music(a_music* music){
    ALenum state = a_get_music_state(music);
    if(state == AL_PAUSED){
        alSourcePlay(music->stream.source);
    }
}

void a_pause_music(a_music* music){
    alSourcePause(music->stream.source);
}

int a_get_music_state(a_music* music){
    int state;
    alGetSourcei(music->stream.source, AL_SOURCE_STATE, &state);
    return state;
}

a_source a_create_source(vec3 position, float range, unsigned int buffer){
    unsigned int id;
    alGenSources(1, &id);

    if(&position == NULL){
        position = (vec3){0.0f, 0.0f, 0.0f};
    }

    alSourcefv(id, AL_POSITION, &position.v[0]);
    alSourcef(id, AL_GAIN, 1.0f);
    alSourcei(id, AL_LOOPING, AL_FALSE);

    if(buffer != 0){
        alSourcei(id, AL_BUFFER, buffer);
        return (a_source){id, position, range, 1.0f, true};
    }
    return (a_source){id, position, range, 1.0f, false};
}

//creates a layer-memory dependent audio source
//this allows for a more direct cleanup in the long run
//by forcing the memory to be only used within the layer's bounds
a_source* a_create_layered_source(int layer, vec3 position, float range, unsigned int buffer){
    if(a_layer_count > layer){
        if(a_layers[layer]->sourceCount < MAX_AUDIO_SOURCES_PER_LAYER){
            int lId = a_layers[layer]->sourceCount;
            unsigned int id;

            alGenSources(1, &id);
            a_layers[layer]->sources[lId].id = id;

            if(&position == NULL){
                position = (vec3){0.0f, 0.0f, 0.0f};
            }

            a_layers[layer]->sources[lId].gain = 1.0f;
            a_layers[layer]->sources[lId].position.x = position.x;
            a_layers[layer]->sources[lId].position.y = position.y;
            a_layers[layer]->sources[lId].position.z = position.z;
            a_layers[layer]->sources[lId].range = range;

            alSourcefv(id, AL_POSITION, &position.v[0]);
            alSourcef(id, AL_GAIN, 1.0f);
            alSourcef(id, AL_MAX_DISTANCE, range);

            if(buffer != 0){
                alSourcei(id, AL_BUFFER, buffer);
                a_layers[layer]->sources[lId].hasSound = true;
            }else{
                a_layers[layer]->sources[lId].hasSound = false;
            }

            return &a_layers[layer]->sources[lId];
        }
    }
    return NULL;
}

void a_destroy_source(a_source source){
    alDeleteSources(1, &source.id);
}

void a_play_source(a_source* source){
    alSourcePlay(source->id);
}

void a_pause_source(a_source* source){
    alSourcePause(source->id);
}

void a_stop_source(a_source* source){
    alSourceStop(source->id);
}

ALenum a_source_state(a_source* source){
    ALenum state;
    alGetSourcei(source->id, AL_SOURCE_STATE, &state);
    return state;
}

static int a_get_open_sfx(){
    for(int i=0;i<MAX_QUICK_SFX;++i){
        int buffer;
        alGetSourcei(a_qsfx[i].source.id, AL_BUFFER, &buffer);
        if(buffer == 0){
            return i;
        }
    }
    printf("No open quick sfx slots\n");
    return -1;
}

int a_play_sfx(a_buffer* buffer, a_sound* qs){
    int sfxSlot = a_get_open_sfx();

    if(sfxSlot == -1){
        printf("Unable to play Quick SFX\n");
        return -1;
    }

    if(qs != NULL){
        alSourcef (a_qsfx[sfxSlot].source.id, AL_GAIN, 1.f);
        alSource3f(a_qsfx[sfxSlot].source.id, AL_POSITION, qs->position.x, qs->position.y, qs->position.z);
        alSource3f(a_qsfx[sfxSlot].source.id, AL_VELOCITY, qs->velocity.x, qs->velocity.y, qs->velocity.z);
    }else{
        alSourcef (a_qsfx[sfxSlot].source.id, AL_GAIN, 1.f);
        alSource3f(a_qsfx[sfxSlot].source.id, AL_POSITION, 0.f, 0.f, 0.f);
        alSource3f(a_qsfx[sfxSlot].source.id, AL_VELOCITY, 0.f, 0.f, 0.f);
    }

    alSourcei(a_qsfx[sfxSlot].source.id, AL_BUFFER, buffer->id);
    a_qsfx[sfxSlot].source.hasSound = true;
    a_play_source(&a_qsfx[sfxSlot].source);

    return sfxSlot;
}

void a_pause_sfx(int sfxSlot){
    a_pause_source(&a_qsfx[sfxSlot].source);
}

void a_stop_sfx(int sfxSlot){
    a_stop_source(&a_qsfx[sfxSlot].source);
}

bool a_is_sfx_buffer(int index, a_buffer* buffer){
    int buf;
    alGetSourcei(a_qsfx[index].source.id, AL_BUFFER, &buf);
    return buf == buffer->id;
}

ALenum a_get_sfx_state(int index){
    ALenum state;
    alGetSourcei(a_qsfx[index].source.id, AL_SOURCE_STATE, &state);
    return state;
}

void a_update_sfx(){
    for(int i=0;i<MAX_QUICK_SFX;++i){
        ALenum state = a_get_sfx_state(i);
        if(state == AL_STOPPED){
            int buffer;
            alGetSourcei(a_qsfx[i].source.id, AL_BUFFER, &buffer);
            if(buffer != 0){
                alSourcei(a_qsfx[i].source.id, AL_BUFFER, 0);
                a_qsfx[i].source.hasSound = false;
            }
        }
    }
}
