#ifndef INPUT_H
#define INPUT_H

#ifndef STD_BOOL_H
#define STD_BOOL_H
typedef enum {false=0,true=1} bool;
#endif

#define KEYBINDING_KEY 1
#define KEYBINDING_MOUSE_BUTTON 2
#define KEYBINDING_JOY_AXIS 3
#define KEYBINDING_JOY_BUTTON 4

#define MAX_KEYS 512
#define MAX_CHARS 512
#define MAX_MOUSE_BUTTONS 32

#define MAX_KEY_BINDINGS 64

#define MAX_JOY_AXES 12
#define MAX_JOY_BUTTONS 64

static int joyButtons[MAX_JOY_BUTTONS];
static int joyButtonsThisFrame[MAX_JOY_BUTTONS];
static int joyButtonsLastFrame[MAX_JOY_BUTTONS];

static int   joyButtonsCount = 0;
static int   joyThisFrameCount = 0;
static int   joyLastFrameCount = 0;

static float joyAxes[MAX_JOY_AXES];
static int   joyAxesCount = 0;

static int   joystickId = 0;
static bool  joyConnected = false;

static bool  charTracking = false;

static int   keyCount = 0;
static int   charCount = 0;

static int   lastKeyCount = 0;
static int   concurrentCount = 0;

static int   CONCURRENTKEYS[MAX_KEYS] = {0};
static int   THISFRAME[MAX_KEYS] = {0};
static int   LASTFRAME[MAX_KEYS] = {0};
static char  CHARS[MAX_CHARS] = {0};

static int   mCount = 0;
static int   mLCount = 0;

static int   MOUSETHISFRAME[MAX_MOUSE_BUTTONS] = {0};
static int   MOUSELASTFRAME[MAX_MOUSE_BUTTONS] = {0};

typedef struct KeyBinding {
    const char*  name;
    int          value;
    unsigned int type;
} KeyBinding;

static bool        keyBindingTrack = false;
static KeyBinding* trackedKeyBinding = 0;
static KeyBinding keyBindings[MAX_KEY_BINDINGS];
static int        keyBindingCount = 0;
static bool       hasInputKeyBindings = false;

//mouse
static double mx = 0;
static double my = 0;

//deltas
static double mdx = 0;
static double mdy = 0;

//scroll deltas
static double msx = 0;
static double msy = 0;

void   i_create_joy(int joyId);
void   i_destroy_joy(int joyId);

static void   i_add_joy_button(int button);
static void   i_rm_joy_button(int button);

float  i_get_joy_axis(int axis);

bool   i_joy_button_down(int button);
bool   i_joy_button_up(int button);
bool   i_joy_button_clicked(int button);
bool   i_joy_button_released(int button);

static void   i_rm_concurrent_key(int index);
void   i_key_callback(int key, int scancode, bool toggle);
bool   i_key_down(int key);
bool   i_key_up(int key);
bool   i_key_clicked(int key);
bool   i_key_released(int key);

void   i_set_char_tracking(bool tracking);
void   i_char_callback(unsigned int c);
char*  i_get_chars();

void   i_mouse_button_callback(int button);
void   i_mouse_pos_callback(double x, double y);
void   i_mouse_scroll_callback(double sx, double sy);

void   i_get_scroll(double* x, double* y);
double i_get_scroll_x();
double i_get_scroll_y();

bool   i_mouse_down(int button);
bool   i_mouse_up(int button);
bool   i_mouse_clicked(int button);
bool   i_mouse_released(int button);

void   i_get_mouse_pos(double* x, double* y);
double i_get_mouse_x();
double i_get_mouse_y();

void   i_get_moues_delta(double* x, double* y);
double i_get_delta_x();
double i_get_delta_y();

void   i_add_binding(const char* name, int value, int type);
void   i_enable_binding_track(const char* keyBinding);
bool   i_binding_track();
void   i_binding_track_callback(int value, int type);
int    i_get_binding_type(const char* keyBinding);
bool   i_binding_clickedi(const char* keyBinding);
bool   i_bining_releasedi(const char* keyBinding);
bool   i_binding_downi(const char* keyBinding);
bool   i_binding_upi(const char* keyBinding);
float  i_binding_val(const char* keyBinding); //gets the value
bool          i_binding_defined(const char* keyBinding);

float i_opposing(const char* prim, const char* sec);

void   i_default_bindings();

void          i_update();

#endif
