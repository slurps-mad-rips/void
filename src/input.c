#ifndef INPUT_C
#define INPUT_C

#include "input.h"
#include <GLFW/glfw3.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void i_create_joy(int joyId){
    if(!joyConnected){
        int present = glfwJoystickPresent(joyId);
        if(present == 0){
            return;
        }
        joystickId = joyId;
    }
}

void i_destroy_joy(int joyId){
    if(joyId == joystickId){
        if(joyConnected){
            joyConnected = false;
        }
    }
}

void i_add_joy_button(int button){
    if(joyButtonsCount == MAX_JOY_BUTTONS){
        return;
    }

    joyButtons[joyButtonsCount] = button;
    joyButtonsCount ++;
}

void i_rm_joy_button(int button){
    if(joyButtonsCount == 0){
        return;
    }

    if(joyButtonsCount == 1){
        joyButtons[0] = 0;
        joyButtonsCount = 0;
        return;
    }

    int index = 0;
    for(int i=0;i<joyButtonsCount;i++){
        if(joyButtons[i] == button){
            index = i;
            break;
        }
    }

    for(int i=index;i<joyButtonsCount-1;i++){
        joyButtons[i] = joyButtons[i+1];
    }

    joyButtonsCount --;
}

float i_get_joy_axis(int axis){
    if(axis > joyAxesCount){
        return 0.0f;
    }
    return joyAxes[axis];
}

bool i_joy_button_down(int button){
    if(button > joyButtonsCount){
        return false;
    }
    return (joyButtons[button] == 1) ? true : false;
}

bool i_joy_button_up(int button){
    if(button > joyButtonsCount){
        return true;
    }
    return (joyButtons[button] == 0) ? true : false;
}

bool i_joy_button_clicked(int button){
    if(button > joyLastFrameCount){
        return false;
    }
    return i_joy_button_down(button);
}

bool i_joy_button_released(int button){
    if(i_joy_button_down(button)){
        return false;
    }
    if(button > joyThisFrameCount){
        return false;
    }

    return (joyButtonsLastFrame[button] == 1) ? true : false;
}

void i_rm_concurrent_key(int index){
    for(int i=index;i<concurrentCount - 1; i++){
        CONCURRENTKEYS[i] = CONCURRENTKEYS[i+1];
    }
}

void i_key_callback(int key, int scancode, bool toggle){
    if(toggle){
        if(keyBindingTrack){
            i_binding_track_callback(key, KEYBINDING_KEY);
        }

        if(concurrentCount == MAX_KEYS){
            return;
        }

        for(int i=0;i<concurrentCount;i++){
            if(CONCURRENTKEYS[i] == key){
                return;
            }
        }

        CONCURRENTKEYS[concurrentCount] = key;
        concurrentCount ++;
    }else{
        int index = 0;
        while(index < concurrentCount){
            if(CONCURRENTKEYS[index] == key){
                i_rm_concurrent_key(index);
                concurrentCount --;
            }
            index++;
        }
    }
}

void i_set_char_tracking(bool tracking){
    charTracking = tracking;
}

void i_char_callback(unsigned int c){
    if(charTracking == false){
        return;
    }

    CHARS[charCount] = c;
    charCount ++;
}

char* i_get_chars(){
    if(charCount == 0){
        return NULL;
    }

    char* chars = calloc(1, charCount);
    for(int i=0;i<charCount;i++){
        chars[i] = CHARS[i];
        CHARS[i] = 0;
    }
    charCount = 0;
    return chars;
}

void i_mouse_button_callback(int button){
    if(keyBindingTrack){
        i_binding_track_callback(button, KEYBINDING_MOUSE_BUTTON);
    }
    MOUSETHISFRAME[mCount] = button;
    mCount++;
}

void i_mouse_pos_callback(double x, double y){
    mdx = x - mx;
    mdy = y - my;
    mx = x;
    my = y;
}

void i_mouse_scroll_callback(double sx, double sy){
    msx = sx;
    msy = sy;
}

void i_get_scroll(double* x, double* y){
    *x = msx;
    *y = msy;
}

bool i_mouse_down(int button){
    for(int i=0;i<mCount;i++){
        if(MOUSETHISFRAME[i] == button){
            return true;
        }
    }
    return false;
}

bool i_mouse_up(int button){
    for(int i=0;i<mCount;i++){
        if(MOUSETHISFRAME[i] == button){
            return false;
        }
    }
    return true;
}

bool i_mouse_clicked(int button){
    for(int i=0;i<mLCount;i++){
        if(MOUSELASTFRAME[i] == button){
            return false;
        }
    }

    for(int i=0;i<mCount;i++){
        if(MOUSETHISFRAME[i] == button){
            return true;
        }
    }
    return false;
}

bool i_mouse_released(int button){
    for(int i=0;i<mCount;i++){
        if(MOUSETHISFRAME[i] == button){
            return false;
        }
    }

    for(int i=0;i<mLCount;i++){
        if(MOUSELASTFRAME[i] == button){
            return true;
        }
    }
    return false;
}

double getScrollX(){
    return msx;
}

double i_get_scroll_y(){
    return msy;
}

void i_get_mouse_pos(double* x, double* y){
    *x = mx;
    *y = my;
}

double i_get_mouse_x(){
    return mx;
}

double i_get_mouse_y(){
    return my;
}

void i_get_moues_delta(double* x, double* y){
    *x = mdx;
    *y = mdy;
}

double i_get_delta_x(){
    return mdx;
}

double i_get_delta_y(){
    return mdy;
}

bool i_key_down(int key){
    for(int i=0;i<keyCount;i++){
        if(THISFRAME[i] == key){
            return true;
        }
    }
    return false;
}

bool i_key_up(int key){
    for(int i=0;i<keyCount;i++){
        if(THISFRAME[i] == key){
            return false;
        }
    }
    return true;
}

bool i_key_clicked(int key){
    for(int i=0;i<lastKeyCount;i++){
        if(LASTFRAME[i] == key){
            return false;
        }
    }

    for(int i=0;i<keyCount;i++){
        if(THISFRAME[i] == key){
            return true;
        }
    }
    return false;
}

bool i_key_released(int key){
    for(int i=0;i<keyCount;i++){
        if(THISFRAME[i] == key){
            return true;
        }
    }

    for(int i=0;i<lastKeyCount;i++){
        if(LASTFRAME[i] == key){
            return true;
        }
    }

    return false;
}

void i_add_binding(const char* name, int value, int type){
    keyBindings[keyBindingCount] = (KeyBinding){name, value, type};
    keyBindingCount ++;
}

void i_enable_binding_track(const char* keyBinding){
    for(int i=0;i<keyBindingCount;i++){
        if(strcmp(keyBindings[i].name, keyBinding) == 0){
            trackedKeyBinding = &keyBindings[i];
            break;
        }
    }

    if(trackedKeyBinding != NULL){
        keyBindingTrack = true;
    }
}

bool i_binding_track(){
    return keyBindingTrack;
}

void i_binding_track_callback(int value, int type){
    if(trackedKeyBinding != NULL){
        trackedKeyBinding->value = value;
        trackedKeyBinding->type = type;
    }

    keyBindingTrack = false;
}

int i_get_binding_type(const char* keyBinding){
    for(int i=0;i<keyBindingCount;i++){
        if(strcmp(keyBindings[i].name, keyBinding) == 0){
            return keyBindings[i].type;
        }
    }
    return 0;
}

bool i_binding_clickedi(const char* keyBinding){
    for(int i=0;i<keyBindingCount;i++){
        if(strcmp(keyBindings[i].name, keyBinding) == 0){
            if(keyBindings[i].type == KEYBINDING_JOY_AXIS){
                return false;
            }
            switch(keyBindings[i].type){
                case KEYBINDING_JOY_BUTTON:
                    return i_joy_button_clicked(keyBindings[i].value);
                case KEYBINDING_KEY:
                    return i_key_clicked(keyBindings[i].value);
                case KEYBINDING_MOUSE_BUTTON:
                    return i_mouse_clicked(keyBindings[i].value);
            }
        }
    }

    return false;
}

bool i_bining_releasedi(const char* keyBinding){
    for(int i=0;i<keyBindingCount;i++){
        if(strcmp(keyBindings[i].name, keyBinding) == 0){
            if(keyBindings[i].type == KEYBINDING_JOY_AXIS){
                return false;
            }

            switch(keyBindings[i].type){
                case KEYBINDING_JOY_BUTTON:
                    return i_joy_button_released(keyBindings[i].value);
                case KEYBINDING_KEY:
                    return i_key_released(keyBindings[i].value);
                case KEYBINDING_MOUSE_BUTTON:
                    return i_mouse_released(keyBindings[i].value);
            }
        }
    }

    return false;
}

bool i_binding_downi(const char* keyBinding){
    for(int i=0;i<keyBindingCount;i++){
        if(strcmp(keyBindings[i].name, keyBinding) == 0){
            if(keyBindings[i].type == KEYBINDING_JOY_AXIS){
                return false;
            }

            switch(keyBindings[i].type){
                case KEYBINDING_JOY_BUTTON:
                    return i_joy_button_down(keyBindings[i].value);
                case KEYBINDING_KEY:
                    return i_key_down(keyBindings[i].value);
                case KEYBINDING_MOUSE_BUTTON:
                    return i_mouse_down(keyBindings[i].value);
            }
        }
    }

    return false;
}

bool i_binding_upi(const char* keyBinding){
    for(int i=0;i<keyBindingCount;i++){
        if(strcmp(keyBindings[i].name, keyBinding) == 0){
            if(keyBindings[i].type == KEYBINDING_JOY_AXIS){
                return false;
            }

            switch(keyBindings[i].type){
                case KEYBINDING_JOY_BUTTON:
                    return i_joy_button_up(keyBindings[i].value);
                case KEYBINDING_MOUSE_BUTTON:
                    return i_mouse_up(keyBindings[i].value);
                case KEYBINDING_KEY:
                    return i_key_up(keyBindings[i].value);
            }
        }
    }
    return false;
}

float i_binding_val(const char* keyBinding){
    for(int i=0;i<keyBindingCount;i++){
        if(strcmp(keyBindings[i].name, keyBinding) == 0){
            switch(keyBindings[i].type){
                case KEYBINDING_MOUSE_BUTTON:
                    return (i_mouse_down(keyBindings[i].value)) ? 1.0f : 0.0f;
                case KEYBINDING_KEY:
                    return (i_key_down(keyBindings[i].value)) ? 1.0f : 0.0f;
                case KEYBINDING_JOY_AXIS:
                    return i_get_joy_axis(keyBindings[i].value);
                case KEYBINDING_JOY_BUTTON:
                    return (i_joy_button_down(keyBindings[i].value)) ? 1.0f : 0.0f;
            }
        }
    }

    return 0.0f;
}

bool i_binding_defined(const char* keyBinding){
    for(int i=0;i<keyBindingCount;i++){
        if(strcmp(keyBindings[i].name, keyBinding) == 0){
            return true;
        }
    }
    return false;
}

float i_opposing(const char* prim, const char* sec){
    float prim_f = i_binding_downi(prim) ? 2.f : 0.f;
    float sec_f = i_binding_downi(sec) ? -1.f : 0.f;
    return prim_f + sec_f;
}

void i_default_bindings(){
    if(!hasInputKeyBindings){
        if(!i_binding_defined("left")){
            i_add_binding("left", GLFW_KEY_A, KEYBINDING_KEY);
        }

        if(!i_binding_defined("right")){
            i_add_binding("right", GLFW_KEY_D, KEYBINDING_KEY);
        }

        if(!i_binding_defined("forward")){
            i_add_binding("forward", GLFW_KEY_W, KEYBINDING_KEY);
        }

        if(!i_binding_defined("back")){
            i_add_binding("back", GLFW_KEY_S, KEYBINDING_KEY);
        }

        if(!i_binding_defined("interact")){
            i_add_binding("interact", GLFW_KEY_F, KEYBINDING_KEY);
        }

        if(!i_binding_defined("inventory")){
            i_add_binding("inventory", GLFW_KEY_TAB, KEYBINDING_KEY);
        }

        if(!i_binding_defined("run")){
            i_add_binding("run", GLFW_KEY_LEFT_SHIFT, KEYBINDING_KEY);
        }

        if(!i_binding_defined("space")){
            i_add_binding("space", GLFW_KEY_SPACE, KEYBINDING_KEY);
        }
    }
}

void i_update(){
    if(!joyConnected){
        for(int i=0;i<16;++i){
            if(glfwJoystickPresent(i)){
                i_create_joy(i);
                printf("Joystick found\n");
                joyConnected = true;
                break;
            }
        }
    }

    bool continuable = true;
    for(int i=0;i<MAX_KEYS;i++){
        if(i < keyCount){
            LASTFRAME[i] = THISFRAME[i];
            continuable = true;
        }else if(i < lastKeyCount){
            LASTFRAME[i] = 0;
            continuable = true;
        }

        if(i < concurrentCount){
            THISFRAME[i] = CONCURRENTKEYS[i];
            continuable = true;
        }else{
            continuable = false;
        }

        if(!continuable){
            break;
        }
    }
    lastKeyCount = keyCount;
    keyCount = concurrentCount;

    for(int i=0;i<MAX_MOUSE_BUTTONS;i++){
        if(i < mCount){
            LASTFRAME[i] = THISFRAME[i];
            THISFRAME[i] = 0;
        }else if(i < mLCount){
            LASTFRAME[i] = 0;
        }else{
            break;
        }
    }

    mLCount = mCount;
    mCount = 0;

    if(joyConnected){
        int count;
        const float* axes = glfwGetJoystickAxes(joystickId, &count);

        for(int i=0;i<MAX_JOY_AXES;i++){
            if(keyBindingTrack == true){
                if(axes[i] != 0.0f){
                    i_binding_track_callback(i, KEYBINDING_JOY_AXIS);
                }
            }

            if(i > count){
                joyAxes[i] = 0.0f;
            }else if(i < count){
                joyAxes[i] = axes[i];
            }
        }
        joyAxesCount = count;

        joyButtonsCount = 0;

        int buttonCount;
        const unsigned char* buttons = glfwGetJoystickButtons(joystickId, &buttonCount);

        for(int i=0;i<MAX_JOY_BUTTONS;++i){
            if(i < joyThisFrameCount){
                joyButtonsLastFrame[i] = joyButtonsThisFrame[i];
            }else{
                joyButtonsLastFrame[i] = 0;
            }
        }

        joyLastFrameCount = joyThisFrameCount;

        for(int i=0;i<buttonCount;i++){
            if(keyBindingTrack == true){
                if(buttons[i] == GLFW_PRESS || buttons[i] == GLFW_REPEAT){
                    i_binding_track_callback(i, KEYBINDING_JOY_BUTTON);
                }
            }

            if(buttons[i] == GLFW_PRESS || buttons[i] == GLFW_REPEAT){
                joyButtons[i] = 1;
                joyButtonsCount ++;
            }else {
                joyButtons[i] = 0;
            }

            joyButtonsThisFrame[i] = joyButtons[i];
        }
        joyThisFrameCount = joyButtonsCount;
    }
}

#endif
