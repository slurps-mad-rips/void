#ifndef GAME_H
#define GAME_H

#define G_MAX_ENTITIES 128
#define G_ENTITY_EXPANSION_RATE 8

#include "types.h"

#include "audio.h"
#include "render.h"

typedef struct g_anim {
    unsigned int id;
    const char* name;
    unsigned int* ids;
} g_anim;

//source
typedef struct g_entity_def {
    unsigned int  entity_id;
    unsigned int* animations;
    unsigned int  animation_count;
    vec2 size;
    unsigned int* sounds;
    unsigned int  sound_count;
} g_entity_def;

//spawn
typedef struct g_entity_spawn {
    vec2 pos;
    unsigned int entity_id;
    void* entity_data;
} e_entity_spawn;

//runtime
typedef struct g_entity {
    vec2 pos;
    unsigned int entity_id;
} g_entity;

typedef struct g_level_info {
    g_entity_spawn* spawns;
    unsigned int spawn_count;

    g_animation* animations;
    unsigned int animation_count;

    a_effect* effects;
    unsigned int effects_count;
} g_level_info;

typedef struct g_level {
    unsigned int entity_size;
    void* entity_spawns;
} g_level;

typedef struct g_shader_lib {
   g_shader_pair* shaders;
   unsigned int shader_count;
} g_shader_lib;

typedef struct g_audio_lib {
    a_music* music;
    unsigned int music_count;

    a_effect* effect_count;
    unsigned int effect_count;
} g_audio_lib;

typedef struct g_anim_lib {
    g_anim* anims;
    unsigned int anim_count;
} g_anim_lib;


g_shader_lib g_load_shader_lib(f_pointer pointer);
g_audio_lib  g_load_audio_lib(f_pointer pointer);
g_anim_lib   g_load_anim_lib(f_pointer pointer);

g_level_info g_load_level_info(f_pointer file);


#endif
