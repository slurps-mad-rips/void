#ifndef USER_H
#define USER_H

#include <toml.h>
#include "files.h"

typedef struct u_toml_conf {
    toml_table_t* table;
    f_pointer file;
    int is_sub;
} u_toml_conf;

typedef struct u_game_save {
    int progression;
    int vals;
} u_game_save;

typedef struct u_game_prefs {
    int save_exists;
    int hardmode;
    f_pointer save_path;
} u_game_prefs;

typedef struct u_render_prefs {
    int shader_quality;
    int width, height;
    bool fullscreen;
    float gamma; //TODO implement gamma correction
} u_render_prefs;

typedef struct u_audio_prefs {
    float music_gain;
    float sfx_gain;
    const char* device;
} u_audio_prefs;

u_toml_conf u_load_conf(f_pointer p);
double u_getd(u_toml_conf conf, const char* value);
int    u_getslen(u_toml_conf conf, const char* value);
void   u_gets(u_toml_conf conf, const char* value, char** ptr);
long   u_getl(u_toml_conf conf, const char* value);
u_toml_conf u_get_sub_conf(u_toml_conf conf, const char* name);
void   u_clean_conf(u_toml_conf conf);

u_game_save    u_load_save(f_pointer p, bool hotload);
u_game_prefs   u_load_prefs(f_pointer p, bool hotload);
u_render_prefs u_load_render_prefs(f_pointer p, bool hotload);
u_audio_prefs  u_load_audio_prefs(f_pointer p, bool hotload);

#endif
