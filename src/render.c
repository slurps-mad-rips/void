#include "render.h"

#include "util.h"

#include "tmath.h"
#include "input.h"

#include <stdio.h>
#include <string.h>

#include "files.h"

static r_window_info g_window_info;
static GLFWmonitor* r_default_monitor;

static const GLFWvidmode* r_vidmodes;
static int r_vidmode_count;

static bool r_allowed = false;

static const char* r_window_title = "Untitled";

static bool r_init_tex_mappings(int length){
    tex_mappings = (r_texmapping*)malloc(sizeof(r_texmapping) * length);

    if(!tex_mappings){
        printf("Unable to allocate texture map of length: %i\n", length);
        return false;
    }

    tex_mapping_capacity = length;
    return true;
}

static void r_rm_tex_mapping(char name[TEX_MAPPING_LEN]){
    if(tex_mapping_count == 0){
        return;
    }

    for(int i=0;i<tex_mapping_count;++i){
        if(strcmp(tex_mappings[i].name, name) == 0){
            r_rm_tex_mappingi(i);
            return;
        }
    }
}

static void r_rm_tex_mappingi(int index){
    if(index > tex_mapping_count){
        printf("Unable to remove past index of: %i, requested %i\n", tex_mapping_count, index);
        return;
    }

    for(int i=index;i<tex_mapping_count;++i){
        tex_mappings[i] = tex_mappings[i+1];
    }

    tex_mappings[tex_mapping_count-1] = (r_texmapping){0};
    tex_mapping_count --;
}

static int r_get_tex_mapping_index(unsigned int id){
    if(tex_mapping_count == 0){
        return -1;
    }

    for(int i=0;i<tex_mapping_count;++i){
        if(tex_mappings[i].texture->id == id){
            return i;
        }
    }

    return -1;
}

r_anim r_create_animation(r_texsheet* sheet, int* frames, int frame_count, int frame_rate, char* name){
    r_anim anim;
    strcpy(anim.name, name);
    anim.frames = frames;
    anim.frame_count = frame_count;
    anim.frame_rate = frame_rate;

    return anim;
}

r_anim_viewer r_view_animation(r_anim* anim, int start_frame, int end_frame){
    return (r_anim_viewer){anim, start_frame, start_frame, end_frame, false};
}

void r_update_animation(r_anim_viewer* viewer, double delta){
    float frame_len = 1.f / viewer->animation->frame_rate;
    float rem =  (viewer->time + delta) - (frame_len * viewer->current_frame);

    if(rem < 0){
        int offset = rem / frame_len;
        rem -= offset * frame_len;
        viewer->current_frame += offset;
    } else {
        viewer->time += delta;
    }

    if(viewer->current_frame >= viewer->end_frame){
        int offset = viewer->current_frame - viewer->end_frame;
        viewer->current_frame = viewer->start_frame + (offset % (viewer->end_frame - viewer->start_frame));
    }else{
        viewer->time += delta;
    }
}

void r_create_camera(r_camera* cam){
    if(cam->size.x == 0 && cam->size.y == 0)
        cam->size = (vec2){160.f, 90.f};

    if(cam->near == 0.f)
        cam->near = -1.f;

    if(cam->far == 0.f)
        cam->far  = 20.f;

    r_update_camera_projection(cam);
    r_update_camera(cam);
}

void r_update_camera_projection(r_camera* cam){
    mat4_identity(&cam->proj);
    mat4_ortho(&cam->proj, 0.f, cam->size.x, 0.f, cam->size.y, cam->near, cam->far);
    // mat4_perspective(&cam->proj, deg2rad(60.f), 1280.f / 720.f, 0.1f, 100.f);
}

void r_update_camera(r_camera* cam){
    mat4_identity(&cam->view);
    mat4_translate(&cam->view, cam->pos.x, cam->pos.y, cam->pos.z);
    mat4_rotate_x(&cam->view, deg2rad(cam->rot.x));
    mat4_rotate_y(&cam->view, deg2rad(cam->rot.y));
}

vec3 r_get_hex_color(const char* hex){
    int len = strlen(hex);
    int start = 0;

    if(len == 4){
        len = 3;
        start = 1;
    }else if(len == 7){
        len = 6;
        start = 1;
    }

    vec3 val = {0.f};

    if(len == 6){
        for(int i=start;i<len;i+=2){
            int res = (int)strtol(&hex[i], NULL, 16);
            val.v[(i-start) / len] = res / 255.f;
        }
    }else if(len == 3){
        for(int i=start;i<len;++i){
            int res = (int)strtol(&hex[i], NULL, 8);
            val.v[(i-start) / len] = res / 255.f;
        }
    }else{
        printf("Incorrect length of hex string: %i\n", len);
    }
}

r_tex* r_get_tex(char name[TEX_MAPPING_LEN]){
    for(int i=0;i<tex_mapping_count;++i){
        if(strcmp(tex_mappings[i].name, name) == 0){
            return tex_mappings[i].texture;
        }
    }

    return NULL;
}

r_tex r_load_tex(char* path){
    int w, h, c;
    unsigned char* data = stbi_load(path, &w, &h, &c, 4);

    GLuint id;

    glGenTextures(1, &id);

    glBindTexture(GL_TEXTURE_2D, id);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, w, h, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);

    glBindTexture(GL_TEXTURE_2D, 0);

    stbi_image_free(data);

    return (r_tex) {id, w, h};
}

void r_bind_tex(r_tex tex){
    glBindTexture(GL_TEXTURE_2D, tex.id);
}

void r_destroy_tex(r_tex* tex){
    if(tex->id == 0){
        return;
    }
    if(tex_mapping_count > 0){
        r_rm_tex_mappingi(r_get_tex_mapping_index(tex->id));
    }

    glDeleteTextures(1, &tex->id);
}

void r_assign_tex(r_tex* tex, char name[TEX_MAPPING_LEN]){
    if(tex_mapping_capacity == tex_mapping_count){
        //we need to expand the buffer
        r_texmapping* temp = (r_texmapping*)malloc(sizeof(r_texmapping) * tex_mapping_capacity + TEX_MAPPING_EXPANSION_RATE);
        memcpy(temp, tex_mappings, sizeof(r_texmapping) * tex_mapping_count);
        free(tex_mappings);
        tex_mappings = temp;
        tex_mapping_capacity += TEX_MAPPING_EXPANSION_RATE;
    }
    r_texmapping mapping;
    strncpy(mapping.name, name, TEX_MAPPING_LEN);
    mapping.texture = tex;
    tex_mappings[tex_mapping_count] = mapping;
    tex_mapping_count++;
}

r_texsheet r_create_tex_sheet(r_tex* tex, unsigned int subwidth, unsigned int subheight){
    return (r_texsheet){tex, subwidth, subheight};
}

void r_get_sub_texcoords(r_texsheet* sheet, unsigned int id, float* coords){
    int max_x = sheet->subwidth / sheet->tex->width;
    int max_y = sheet->subheight / sheet->tex->height;

    int max = max_x * max_y;

    if(id > max){
        printf("ID of %i out of range: %i\n", id, max);
        return;
    }

    int x = id % max_x;
    int y = id / max_x;

    float ox = x * sheet->subwidth / sheet->tex->width;
    float oy = y * sheet->subheight / sheet->tex->height;

    float x0 = x * ox;
    float x1 = x0 + ox;

    float y0 = y * oy;
    float y1 = y0 + oy;

    coords[0] = x0;
    coords[1] = y0;

    coords[2] = x1;
    coords[3] = y0;

    coords[4] = x1;
    coords[5] = y1;

    coords[6] = x0;
    coords[7] = y1;
}

r_quad r_create_quad(float width, float height){
    float hw = width / 2.f;
    float hh = height / 2.f;

    float vertices[12] = {
        -hw,  hh, 0.f,
        hw,  hh, 0.f,
        hw, -hh, 0.f,
        -hw, -hh, 0.f
    };

    float texcoords[8] = {
        0.0f, 0.0f,
        1.0f, 0.0f,
        1.0f, 1.0f,
        0.0f, 1.0f
    };

    unsigned short indices[6] = {
        0, 1, 2,
        2, 3, 0
    };

    unsigned int vao;
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);

    unsigned int vbo;
    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(float) * 12, &vertices[0], GL_STATIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, 0, 0, 0);
    glEnableVertexAttribArray(0);

    unsigned int vto;
    glGenBuffers(1, &vto);
    glBindBuffer(GL_ARRAY_BUFFER, vto);
    glBufferData(GL_ARRAY_BUFFER, sizeof(float) * 8, &texcoords[0], GL_STATIC_DRAW);
    glVertexAttribPointer(1, 2, GL_FLOAT, 0, 0, 0);
    glEnableVertexAttribArray(1);

    unsigned int vboi;
    glGenBuffers(1, &vboi);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vboi);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned short) * 6, &indices[0], GL_STATIC_DRAW);

    glBindVertexArray(0);

    return (r_quad){vao, vbo, vto, vboi, width, height};
}

void r_destroy_quad(r_quad* quad){
    glDeleteVertexArrays(1, &quad->vao);
}

void r_draw_quad(r_quad* quad){
    glBindVertexArray(quad->vao);

    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);

    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_SHORT, 0);

    glDisableVertexAttribArray(0);
    glDisableVertexAttribArray(1);

    glBindVertexArray(0);
}

static GLuint r_get_sub_shader(const char* filePath, int type){
    char* text = (char*)f_get_data((f_pointer){filePath});
    GLint success = 0;
    GLuint id = glCreateShader(type);

    const char* ptr = text;

    glShaderSource(id, 1, &ptr, NULL);
    glCompileShader(id);

    bool r = r_no_err();
    if(!r){
        printf("r_shader compilation: %s\n", (type == GL_VERTEX_SHADER) ? "vertex" : "fragment");
        r_no_err();
    }

    glGetShaderiv(id, GL_COMPILE_STATUS, &success);
    if(success != GL_TRUE){
        int maxlen = 0;
        int len;
        glGetShaderiv(id, GL_INFO_LOG_LENGTH, &maxlen);

        char* log = malloc(maxlen);

        glGetShaderInfoLog(id, maxlen, &len, log);

        printf("%s\n", log);
        free(log);
    }

    free(text);

    return id;
}


bool r_no_err(){
    GLenum err = GL_NO_ERROR;
    err = glGetError();
    if(err != GL_NO_ERROR){
        printf("gl error: %i\n", err);
        return false;
    }
    return true;
}

void r_loop_err(){
    GLenum err = GL_NO_ERROR;
    err = glGetError();
    int count = 0;
    while(err != GL_NO_ERROR){
        printf("OpenGL Error: %i\n", err);
        err = glGetError();
        count ++;
    }
    if(count > 0) printf("End of GL Errors.\n");
}

r_shader r_create_shader(const char* vert, const char* frag){
    GLuint v = r_get_sub_shader(vert, GL_VERTEX_SHADER);
    GLuint f = r_get_sub_shader(frag, GL_FRAGMENT_SHADER);

    GLuint id = glCreateProgram();

    glAttachShader(id, v);
    glAttachShader(id, f);

    bool r = r_no_err();
    if(!r) printf("shader attachment\n");

    glLinkProgram(id);

    r = r_no_err();
    if(!r) printf("shader linkage\n");
    GLint success;
    glGetProgramiv(id, GL_LINK_STATUS, &success);
    if(success != GL_TRUE){
        int maxlen = 0;
        int len;
        glGetProgramiv(id, GL_INFO_LOG_LENGTH, &maxlen);
        char* log = malloc(maxlen);
        glGetProgramInfoLog(id, maxlen, &len, log);
        printf("%s\n", log);
        free(log);
    }

#ifdef DEBUG_OUTPUT
    printf("r_shader program loaded: %i\n", id);
#endif

    return (r_shader){id};
}


void r_destroy_shader(r_shader* shader){
    glDeleteProgram(shader->id);
}

void r_bind_shader(r_shader* shader){
    if(shader == NULL){
        glUseProgram(0);
    }else{
        glUseProgram(shader->id);
    }
}

int r_get_uniform_loc(r_shader shader, const char* uniform){
    return glGetUniformLocation(shader.id, uniform);
}

void r_set_uniformf(r_shader shader, const char* name, float value){
    glUniform1f(r_get_uniform_loc(shader, name), value);
}

void r_set_uniformi(r_shader shader, const char* name, int value){
    glUniform1i(r_get_uniform_loc(shader, name), value);
}

void r_set_vec4(r_shader shader, const char* name, vec4 value){
    glUniform4f(r_get_uniform_loc(shader, name), value.x, value.y, value.z, value.w);
}

void r_set_vec3(r_shader shader, const char* name, vec3 value){
    glUniform3f(r_get_uniform_loc(shader, name), value.x, value.y, value.z);
}

void r_set_vec2(r_shader shader, const char* name, vec2 value){
    glUniform2f(r_get_uniform_loc(shader, name), value.x, value.y);
}

void r_set_quat(r_shader shader, const char* name, quat value){
    glUniform4f(r_get_uniform_loc(shader, name), value.x, value.y, value.z, value.w);
}

void r_set_mat4(r_shader shader, const char* name, mat4 value){
    glUniformMatrix4fv(r_get_uniform_loc(shader, name), 1, GL_FALSE, &value.v[0][0]);
}

static void r_create_modes(){
    if(r_default_monitor == NULL){
        r_default_monitor = glfwGetPrimaryMonitor();
    }

    r_vidmodes = glfwGetVideoModes(r_default_monitor, &r_vidmode_count);
}

static bool r_window_info_valid(r_window_info info){
    return info.width > 0 && info.height > 0;
}

static const GLFWvidmode* r_find_closest_mode(r_window_info info){
    if(r_vidmode_count == 0){
        r_create_modes();
    }else if(r_vidmode_count == 1){
        return r_vidmodes;
    }

    const GLFWvidmode* closest = &r_vidmodes[0];
    int distance = (abs(info.width - r_vidmodes[0].width) + abs(info.height - r_vidmodes[0].height) - r_vidmodes[0].refreshRate);

    for(int i=0;i<r_vidmode_count;++i){
        int d2 = (abs(info.width - r_vidmodes[i].width) + abs(info.height - r_vidmodes[i].height) - r_vidmodes[i].refreshRate);
        if(d2 < distance){
            closest = &r_vidmodes[i];
            distance = d2;
        }
    }

    return closest;
}

static const GLFWvidmode* r_find_best_mode(){
    if(r_vidmode_count == 0){
        r_create_modes();
    }else if(r_vidmode_count == 1){
        return r_vidmodes;
    }

    const GLFWvidmode* selected = &r_vidmodes[0];
    int value = selected->width + selected->height * (selected->refreshRate * 2);

    for(int i=0;i<r_vidmode_count;++i){
        int v2 = r_vidmodes[i].width + r_vidmodes[i].height * (r_vidmodes[i].refreshRate * 2);
        if(v2 > value){
            selected = &r_vidmodes[i];
            value = v2;
        }
    }

    return selected;
}

bool r_create_window(r_window_info info){
    if(!info.fullscreen && !r_window_info_valid(info)){
        return false;
    }

    if(!glfwInit()){
#ifdef DEBUG_OUTPUT
        printf("Unable to initialize GLFW\n");
#endif
        return false;
    }

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    GLFWwindow* window = NULL;

    if(info.fullscreen){
        r_create_modes();
        const GLFWvidmode* selected_mode;
        if(r_window_info_valid(info)){
            selected_mode = r_find_closest_mode(info);
        }else{
            selected_mode = r_find_best_mode();
        }

        glfwWindowHint(GLFW_RED_BITS, selected_mode->redBits);
        glfwWindowHint(GLFW_GREEN_BITS, selected_mode->greenBits);
        glfwWindowHint(GLFW_BLUE_BITS, selected_mode->blueBits);
        glfwWindowHint(GLFW_REFRESH_RATE, selected_mode->refreshRate);

        g_window.refreshRate = selected_mode->refreshRate;
        g_window.width = selected_mode->width;
        g_window.height = selected_mode->height;
        g_window.fullscreen = true;
        window = glfwCreateWindow(selected_mode->width, selected_mode->height, r_window_title, r_default_monitor, NULL);

    }else{
        glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);
        glfwWindowHint(GLFW_DECORATED, info.borderless ? GLFW_FALSE : GLFW_TRUE);

        if(info.refreshRate > 0){
            glfwWindowHint(GLFW_REFRESH_RATE, info.refreshRate);
            g_window.refreshRate = info.refreshRate;
        }

        g_window.width = info.width;
        g_window.height = info.height;
        g_window.fullscreen = false;
        g_window.vsync = false;

        window = glfwCreateWindow(info.width, info.height, r_window_title, NULL, NULL);
    }

    if(!window){
#ifdef DEBUG_OUTPUT
        printf("Error: Unable to create GLFW window.\n");
#endif
        glfwTerminate();
        return false;
    }

    g_window.glfw = window;

    glfwMakeContextCurrent(window);
    gladLoadGLLoader((GLADloadproc)glfwGetProcAddress);

    r_allowed = true;

    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    //glEnable(GL_TEXTURE_2D);
    glViewport(0, 0, g_window.width, g_window.height);

    glfwGetWindowPos(g_window.glfw, &g_window.x, &g_window.y);

    glDisable(GL_CULL_FACE);

    vec3 color = util_get_color("FFF");
    glClearColor(color.r, color.g, color.b, 1.f);

    glfwSetWindowPosCallback(g_window.glfw, glfw_window_pos_cb);
    glfwSetWindowSizeCallback(g_window.glfw, glfw_window_size_cb);
    glfwSetWindowCloseCallback(g_window.glfw, glfw_window_close_cb);
    glfwSetKeyCallback(g_window.glfw, glfw_key_cb);
    glfwSetCharCallback(g_window.glfw, glfw_char_cb);
    glfwSetMouseButtonCallback(g_window.glfw, glfw_mouse_button_cb);
    glfwSetCursorPosCallback(g_window.glfw, glfw_mouse_pos_cb);
    glfwSetScrollCallback(g_window.glfw, glfw_scroll_cb);
    glfwSetJoystickCallback(glfw_joy_cb);

    i_default_bindings();

    return false;
}

void r_center_window(){
    GLFWmonitor* mon = NULL;
    int monitor_count;
    GLFWmonitor** monitors =  glfwGetMonitors(&monitor_count);

    if(monitor_count == 0){
        return;
    }else if(monitor_count == 1){
        const GLFWvidmode* mode = glfwGetVideoMode(monitors[0]);
        r_set_window_pos((mode->width - g_window.width) / 2, (mode->height - g_window.height) / 2);
        return;
    }

    int mon_x, mon_y;
    int mon_w, mon_h;

    for(int i=0;i<monitor_count;++i){
        glfwGetMonitorPos(monitors[i], &mon_x, &mon_y);
        const GLFWvidmode* mode = glfwGetVideoMode(monitors[i]);
        if(g_window.x > mon_x && g_window.x < mon_x + mode->width){
            if(g_window.y > mon_y && g_window.y < mon_y + mode->height){
                mon_w = mode->width;
                mon_h = mode->height;
                mon = monitors[i];
                break;
            }
        }
    }

    if(mon != NULL){
        r_set_window_pos((mon_w - g_window.width) / 2, (mon_h - g_window.height) / 2);
    }
}

void r_set_window_pos(int x, int y){
    glfwSetWindowPos(g_window.glfw, x, y);
}

void r_destroy_window(){
    r_allowed = false;

    glfwDestroyWindow(g_window.glfw);

    g_window.glfw = NULL;
    g_window.width = -1;
    g_window.height = -1;
    g_window.refreshRate = -1;
    g_window.fullscreen = false;
    g_window.vsync = false;
}

void r_request_close(){
    g_window.close_requested = true;
}

bool r_window_should_close(){
    return g_window.close_requested;
}

void r_swap_buffers(){
    glfwSwapBuffers(g_window.glfw);
}

void r_clear_window(){
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void r_poll_window(){
    glfwPollEvents();
}

static void glfw_err_cb(int error, const char* msg){
    printf("ERROR: %i %s\n", error, msg);
}

static void glfw_window_pos_cb(GLFWwindow* window, int x, int y){
    g_window.x = x;
    g_window.y = y;
}

static void glfw_window_size_cb(GLFWwindow* window, int w, int h){
    g_window.width = w;
    g_window.height = h;
    glViewport(0, 0, w, h);
}

static void glfw_window_close_cb(GLFWwindow* window){
    g_window.close_requested = true;
}

static void glfw_key_cb(GLFWwindow* window, int key, int scancode, int action, int mods){
    if(action == GLFW_PRESS || action == GLFW_REPEAT){
        i_key_callback(key, scancode, true);
        if(keyBindingTrack == true){
            i_binding_track_callback(key, KEYBINDING_KEY);
        }
    }else if(action == GLFW_RELEASE){
        i_key_callback(key, scancode, false);
    }
}

static void glfw_char_cb(GLFWwindow* window, unsigned int c){
    i_char_callback(c);
}

static void glfw_mouse_pos_cb(GLFWwindow* window, double x, double y){
    i_mouse_pos_callback(x, y);
}

static void glfw_mouse_button_cb(GLFWwindow* window, int button, int action, int mods){
    if(action == GLFW_PRESS || action == GLFW_REPEAT){
        i_mouse_button_callback(button);
        if(keyBindingTrack == true){
            i_binding_track_callback(button, KEYBINDING_MOUSE_BUTTON);
        }
    }
}

static void glfw_scroll_cb(GLFWwindow* window, double dx, double dy){
    i_mouse_scroll_callback(dx, dy);
}

static void glfw_joy_cb(int joystick, int action){
    if(action == GLFW_CONNECTED){
        i_create_joy(joystick);
    }else if(action == GLFW_DISCONNECTED){
        i_destroy_joy(joystick);
    }
}
