#ifndef FILES_H
#define FILES_H

#include <stdlib.h>
#include <stdio.h>
#include "types.h"

inline void* f_get_data(f_pointer f){
    FILE* file;
    unsigned char* data = NULL;
    int count = 0;
    file = fopen(f.path, "rt");
    if(file != NULL){
        fseek(file, 0, SEEK_END);
        count = ftell(file);
        rewind(file);

        if(count > 0){
            data = malloc(sizeof(unsigned char)*(count+1));
            count = fread(data, sizeof(unsigned char), count, file);
            data[count] = '\0';
        }

        fclose(file);
    }

    return (void*)data;
}

#endif
