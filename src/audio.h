#ifndef AUDIO_H
#define AUDIO_H

#include <AL/alc.h>
#include <AL/al.h>
#include "tmath.h"

#include <stdlib.h>
#include <stdio.h>

#include <stb_vorbis.c>

#include <string.h>

#include "files.h"

#define MAX_AUDIO_SOURCES_PER_LAYER 32 
#define MAX_AUDIO_LAYERS 4

#ifndef STD_BOOL_H
#define STD_BOOL_H
typedef enum{false=0,true=1} bool;
#endif

typedef unsigned int uint;

#define LAYER_STOPPED 1
#define LAYER_PLAYING 2
#define LAYER_PAUSED  3

#define MAX_MUSIC_RUNTIME 4096

#define MAX_QUICK_SFX 8

#define CACHE_AUDIO_FILES

typedef struct a_context {
    ALCcontext* context;
    ALCdevice*  device;
} a_context;

typedef struct a_buffer {
    uint id;
    bool buffered;
    int  channels;
    int  length;
    int  sampleRate;
} a_buffer;

typedef struct a_stream {
    unsigned int sampleRate;
    unsigned int sampleSize;
    unsigned int channels;

    int format;
    unsigned int source;
    unsigned int buffers[2]; 
} a_stream;

typedef struct a_music {
    a_stream stream;
    unsigned int samplesLeft;
    unsigned int totalSamples;
    stb_vorbis* vorbis;
    bool loop;
} a_music;

typedef struct a_source {
    uint  id;
    vec3  position;
    float range, gain;
    bool  hasSound;
    bool  loop;
} a_source;

typedef struct a_layer {
    uint        id;
    const char* name;
    a_source sources[MAX_AUDIO_SOURCES_PER_LAYER];
    int         sourceCount;
    float       gain;
    int         state;
} a_layer;

typedef struct a_sfx {
    unsigned int id;
    a_source source;
} a_sfx;

typedef struct a_sound {
    float gain;
    vec3 position;
    vec3 velocity;
    float range;
} a_sound;

typedef struct a_effect {
    f_pointer path;
    const char* name;
    float length;
} a_effect;

static a_sfx a_qsfx[MAX_QUICK_SFX];
static int a_sfx_playing = 0;

static a_context a_Context;

static char** a_device_list;
static int a_device_list_count = 0;

static bool a_allow = false;

static a_layer* a_layers[MAX_AUDIO_LAYERS];
static int a_layer_count = 0;

#ifdef CACHE_AUDIO_FILES
//psuedo-audio buffer pointer
typedef struct a_file {
    const char* path;
    a_buffer* buffer;
} a_file;

#ifndef MAX_AUDIO_FILES
#define MAX_AUDIO_FILES 256
#endif

static a_file audioFiles[MAX_AUDIO_FILES];
static int    audioFileCount = 0;

a_file*      a_gen_file(const char* fp, a_buffer* buffer);
int          a_get_file_index(const char* fp);
a_file*      a_get_file(const char* fp);
void         a_destroy_file(const char* fp);
a_buffer*    a_get_buffer(const char* fp);
a_file**     a_load_sfx(const char** fp, int count);
void         a_destroy_file_cache();
#endif

bool         a_load_devices();
bool         a_create_context(const char* deviceName);
void         a_destroy_context();
const char** a_get_devices(int* count);
void         a_swap_device(const char* deviceName);

a_buffer     a_create_buffer(const char* path);
a_buffer*    a_create_buffers(const char** paths, int pCount, int* bCount);
void         a_destroy_buffer(a_buffer buffer);
void         a_clean_buffers(a_buffer* buffers, int count);
a_buffer     a_load_effect(a_effect* effect);

void         a_attach_buffer(a_source* src, a_buffer* buf);
void         a_detach_buffer(a_source* src);


a_stream     a_create_stream(int sampleRate, int sampleSize, int channels);
void         a_destroy_stream(a_stream stream);
void         a_update_stream(a_stream stream, const void* data, int samplesCount);
a_music*     a_create_music(const char* path);
bool         a_update_music(a_music* music);
void         a_destroy_music(a_music* music);
float        a_get_music_len_time(a_music* music);
float        a_get_music_time(a_music* music);
void         a_play_music(a_music* music);
void         a_stop_music(a_music* music);
void         a_resume_music(a_music* music);
void         a_pause_music(a_music* music);
ALenum       a_get_music_state(a_music* music);
a_source     a_create_source(vec3 position, float range, unsigned int buffer);
a_source*    a_create_layered_source(int layer, vec3 position, float range, unsigned int buffer);

void         a_destroy_source(a_source source);
void         a_play_source(a_source* source);
void         a_pause_source(a_source* source);
void         a_stop_source(a_source* source);
int          a_source_state(a_source* source);
void         a_clean_sources(a_source* sources, int count);
a_layer*     a_create_layer(const char* name, a_source* sources, int length);

int          a_get_layer_index(const char* name);
void         a_destroy_layer(int index);
void         a_set_layer_gain(int index, float gain);
float        a_get_layer_gain(int index);
void         a_play_layer(int index);
void         a_stop_layer(int index);
void         a_pause_layer(int index); //TODO
void         a_clean_layers(a_layer** layers, int count);

int          a_play_sfx(a_buffer* buffer, a_sound* sound);
void         a_pause_sfx(int index);
void         a_stop_sfx(int index);
bool         a_is_sfx_buffer(int index, a_buffer* buffer);
int          a_get_sfx_state(int index);
void         a_update_sfx();
static int   a_get_open_sfx();

#endif
